#!/bin/sh
#
# This script harnesses the standard libGridXC makefile to build an
# alternative hierarchy of the form (Note simplified version 2021)
#
#                PREFIX/
#                      gridxc.mk
#                      libxc.mk
#                      serial/
#                            lib/
#                            include/
#                      mpi/
#                            lib/
#                            include/
#
# The 'mpi' section is optional, depending on the command issued.
#
# Usage:
#
# WITH_LIBXC=X  WITH_MPI=Y PREFIX=Z sh build.sh
#
# If Y is empty, only the 'serial' section is built, otherwise both
# 'serial' and 'mpi' sections are created. If X is non-empty, libXC
# support is compiled in. Note that there are no extra sections
# devoted to libXC/MPI combinations yet. The user should use separate
# top-levels for this.
#
# If PREFIX is empty, the hierarchy is rooted in the current directory.
# 
# The top-level gridxc.mk has special logic to dispatch the
# appropriate variables depending on the setting of WITH_MPI in client
# makefiles.
#
# Set to "." if empty or unset
inPREFIX=${PREFIX:-.}
#
mkdir -p ${inPREFIX}
#
if [ "${WITH_LIBXC}" != "" ]
then

    #   Install copy of libxc.mk file at the top
    #   and define LIBXC_MK symbol for makefile
    #
    libxc_major_version=$(grep XC_MAJOR_VERSION ${LIBXC_ROOT}/include/xc_version.h | awk '{print $NF}')
    if test ${libxc_major_version} -ge 4
    then
	LIBXC_MK=libxc.mk
	cp -p libxc.mk ${inPREFIX}/libxc.mk
    else
	LIBXC_MK=libxc-pre4.mk
	cp -p libxc-pre4.mk ${inPREFIX}/libxc.mk
    fi
fi

# Build first without MPI
#
echo "==> make LIBXC_MK=${LIBXC_MK} WITH_LIBXC=${WITH_LIBXC} WITH_MPI= PREFIX=${inPREFIX}/serial"
sleep 1
make clean
make LIBXC_MK=${LIBXC_MK} WITH_LIBXC=${WITH_LIBXC} WITH_MPI= PREFIX=${inPREFIX}/serial
#
# Install the top-level gridxc.mk
#
if [ "${WITH_LIBXC}" != "" ]
then
    sed 's/GRIDXC_USES_LIBXC=0/GRIDXC_USES_LIBXC=1/g' top.gridxc.mk.in \
	                    > ${inPREFIX}/gridxc.mk

else
    cp -p top.gridxc.mk.in ${inPREFIX}/gridxc.mk
fi
#
# Build with MPI if requested
#
if [ "${WITH_MPI}" != "" ]
then
   echo "==> make LIBXC_MK=${LIBXC_MK} WITH_LIBXC=${WITH_LIBXC} WITH_MPI=1 PREFIX=${inPREFIX}/mpi"
   sleep 1
   make clean
   make LIBXC_MK=${LIBXC_MK} WITH_LIBXC=${WITH_LIBXC} WITH_MPI=1 PREFIX=${inPREFIX}/mpi
#
# Install the top-level gridxc.mk
#
   if [ "${WITH_LIBXC}" != "" ]
   then
       sed 's/GRIDXC_USES_LIBXC=0/GRIDXC_USES_LIBXC=1/g' top.gridxc.mk.in | \
	   sed 's/GRIDXC_USES_MPI=0/GRIDXC_USES_MPI=1/g'  > ${inPREFIX}/gridxc.mk
   else
       sed 's/GRIDXC_USES_MPI=0/GRIDXC_USES_MPI=1/g' top.gridxc.mk.in \
	                         > ${inPREFIX}/gridxc.mk
   fi
fi
#


