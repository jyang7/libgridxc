# -*- Automake -*-
#
# Makefile for the LibGridXC package
#
# Copyright (C) 2017 Yann Pouillon
#
# This file is part of the LibGridXC software package. For license information,
# please see the COPYING file in the top-level directory of the source
# distribution.
#

                    # ------------------------------------ #

#
# Initial setup
#

AM_CPPFLAGS = @FCFLAGS_LIBXC@

                    # ------------------------------------ #

#
# Main source files
#

# Common source files
gxc_srcs = \
  alloc.F90 \
  am05.F90 \
  array.F90 \
  atomxc.F90 \
  bessph.F90 \
  cellsubs.F90 \
  cellxc.F90 \
  chkgmx.F90 \
  debugxc.F90 \
  fft3d.F90 \
  fftr.F90 \
  ggaxc.F90 \
  gridxc.F90 \
  gridxc_config.F90 \
  interpolation.F90 \
  ldaxc.F90 \
  gridxc_fft_gpfa.F90 \
  m_io.F90 \
  m_walltime.F90 \
  mesh1d.F90 \
  mesh3d.F90 \
  minvec.F90 \
  moreParallelSubs.F90 \
  precision.F90 \
  radfft.F90 \
  sorting.F90 \
  sys.F90 \
  vdwxc.F90 \
  vv_vdwxc.F90 \
  xc_hybrids.F90 \
  xc_xwpbe.F90 \
  xcmod.F90

# Fortran modules
# TODO: support both lower-case and upper-case file names
gxc_f03_mods = \
  gridxc.$(MODEXT) \
  gridxc_debugxc.$(MODEXT) \
  gridxc_config.$(MODEXT) \
  gridxc_atom.$(MODEXT) \
  gridxc_cell.$(MODEXT) \
  gridxc_fft_gpfa.$(MODEXT) \
  gridxc_gga.$(MODEXT) \
  gridxc_lda.$(MODEXT) \
  gridxc_mesh3d.$(MODEXT) \
  gridxc_xcmod.$(MODEXT)

# Internal Fortran modules (not used by external programs)
gxc_f03_int_mods = \
  gridxc_alloc.$(MODEXT) \
  gridxc_am05.$(MODEXT) \
  gridxc_cellsubs.$(MODEXT) \
  gridxc_fftr.$(MODEXT) \
  gridxc_gpfa_core_dp.$(MODEXT) \
  gridxc_gpfa_core_sp.$(MODEXT) \
  gridxc_interpolation.$(MODEXT) \
  gridxc_array.$(MODEXT) \
  gridxc_bessph.$(MODEXT) \
  gridxc_chkgmx.$(MODEXT) \
  gridxc_fft3d.$(MODEXT) \
  gridxc_io.$(MODEXT) \
  gridxc_minvec.$(MODEXT) \
  gridxc_radfft.$(MODEXT) \
  gridxc_vdwxc.$(MODEXT) \
  gridxc_vv_vdwxc.$(MODEXT) \
  gridxc_walltime.$(MODEXT) \
  gridxc_mesh1d.$(MODEXT) \
  gridxc_moreparallelsubs.$(MODEXT) \
  gridxc_precision.$(MODEXT) \
  gridxc_sorting.$(MODEXT) \
  gridxc_sys.$(MODEXT) \
  gridxc_hybrids.$(MODEXT) \
  gridxc_xwpbe.$(MODEXT)

# Libraries to install
lib_LTLIBRARIES = libgridxc@PREC_SUFFIX@@ARCH_SUFFIX@.la

libgridxc@PREC_SUFFIX@@ARCH_SUFFIX@_la_SOURCES = $(gxc_srcs)
libgridxc@PREC_SUFFIX@@ARCH_SUFFIX@_la_LDFLAGS = -version-info 0:0:0
libgridxc@PREC_SUFFIX@@ARCH_SUFFIX@_la_LIBADD =
if GRIDXC_USES_LIBXC
libgridxc@PREC_SUFFIX@@ARCH_SUFFIX@_la_LIBADD += @LIBS_LIBXC@
endif

# Fortran modules to install
f03moddir = $(includedir)/gridxc@PREC_SUFFIX@@ARCH_SUFFIX@
gxc_f03_mods_inst = $(gxc_f03_mods)
if GRIDXC_NEC_COMPAT
gxc_f03_mods_inst += $(gxc_f03_int_mods)
endif
install-data-local:
	$(INSTALL) -d -m 755 $(DESTDIR)$(f03moddir)
	$(INSTALL) -m 644 $(gxc_f03_mods_inst) $(DESTDIR)$(f03moddir)

uninstall-local:
	cd $(DESTDIR)$(f03moddir) && rm -f $(gxc_f03_mods_inst)

# Local cleaning
CLEANFILES = $(gxc_f03_mods) $(gxc_f03_int_mods)

# Keep former build system for now
EXTRA_DIST = \
  build.sh \
  config.sh \
  gridxc.mk.in \
  libxc.mk \
  makefile.alt \
  top.gridxc.mk.in

                    ########################################

# Explicit dependencies within LibGridXC
# Note: this is needed because of Fortran

am05.$(LTOBJEXT): \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

array.$(LTOBJEXT): \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

atomxc.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  debugxc.$(LTOBJEXT) \
  gridxc_config.$(LTOBJEXT) \
  ggaxc.$(LTOBJEXT) \
  ldaxc.$(LTOBJEXT)

atomxc.$(LTOBJEXT): \
  radfft.$(LTOBJEXT) \
  vdwxc.$(LTOBJEXT) \
  mesh1d.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT) \
  xcmod.$(LTOBJEXT)

bessph.$(LTOBJEXT): \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

cellsubs.$(LTOBJEXT): \
  precision.$(LTOBJEXT)

cellxc.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  cellsubs.$(LTOBJEXT) \
  debugxc.$(LTOBJEXT) \
  fftr.$(LTOBJEXT) \
  gridxc_config.$(LTOBJEXT)

cellxc.$(LTOBJEXT): \
  chkgmx.$(LTOBJEXT) \
  ggaxc.$(LTOBJEXT) \
  ldaxc.$(LTOBJEXT) \
  vdwxc.$(LTOBJEXT) \
  mesh3d.$(LTOBJEXT)

cellxc.$(LTOBJEXT): \
  moreParallelSubs.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT) \
  xcmod.$(LTOBJEXT)

chkgmx.$(LTOBJEXT): \
  cellsubs.$(LTOBJEXT) \
  minvec.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

debugxc.$(LTOBJEXT): \
  m_io.$(LTOBJEXT) \
  moreParallelSubs.$(LTOBJEXT)

fft3d.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  debugxc.$(LTOBJEXT) \
  gridxc_fft_gpfa.$(LTOBJEXT) \
  mesh3d.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

fftr.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  fft3d.$(LTOBJEXT) \
  mesh3d.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

ggaxc.$(LTOBJEXT): \
  am05.$(LTOBJEXT) \
  ldaxc.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT) \
  xc_hybrids.$(LTOBJEXT)

gridxc.$(LTOBJEXT): \
  debugxc.$(LTOBJEXT) \
  gridxc_config.$(LTOBJEXT) \
  atomxc.$(LTOBJEXT) \
  cellxc.$(LTOBJEXT) \
  gridxc_fft_gpfa.$(LTOBJEXT)

gridxc.$(LTOBJEXT): \
  ggaxc.$(LTOBJEXT) \
  ldaxc.$(LTOBJEXT) \
  mesh3d.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  xcmod.$(LTOBJEXT)

gridxc_config.$(LTOBJEXT): \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

interpolation.$(LTOBJEXT): \
  precision.$(LTOBJEXT)

ldaxc.$(LTOBJEXT): \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

gridxc_fft_gpfa.$(LTOBJEXT): \
  precision.$(LTOBJEXT)

m_io.$(LTOBJEXT): \
  sys.$(LTOBJEXT)

mesh1d.$(LTOBJEXT): \
  interpolation.$(LTOBJEXT) \
  precision.$(LTOBJEXT)

mesh3d.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  debugxc.$(LTOBJEXT) \
  gridxc_config.$(LTOBJEXT) \
  array.$(LTOBJEXT)

mesh3d.$(LTOBJEXT): \
  precision.$(LTOBJEXT) \
  sorting.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

minvec.$(LTOBJEXT): \
  cellsubs.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sorting.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

moreParallelSubs.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  gridxc_config.$(LTOBJEXT) \
  array.$(LTOBJEXT) \
  m_io.$(LTOBJEXT)

moreParallelSubs.$(LTOBJEXT): \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

radfft.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  bessph.$(LTOBJEXT) \
  gridxc_fft_gpfa.$(LTOBJEXT) \
  precision.$(LTOBJEXT)

sorting.$(LTOBJEXT): \
  precision.$(LTOBJEXT)

vdwxc.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  debugxc.$(LTOBJEXT) \
  interpolation.$(LTOBJEXT) \
  ggaxc.$(LTOBJEXT) \
  ldaxc.$(LTOBJEXT)

vdwxc.$(LTOBJEXT): \
  radfft.$(LTOBJEXT) \
  vv_vdwxc.$(LTOBJEXT) \
  mesh1d.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

vv_vdwxc.$(LTOBJEXT): \
  alloc.$(LTOBJEXT) \
  debugxc.$(LTOBJEXT) \
  interpolation.$(LTOBJEXT) \
  radfft.$(LTOBJEXT) \
  mesh1d.$(LTOBJEXT)

vv_vdwxc.$(LTOBJEXT): \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)

xc_hybrids.$(LTOBJEXT): \
  ldaxc.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  xc_xwpbe.$(LTOBJEXT)

xc_xwpbe.$(LTOBJEXT): \
  precision.$(LTOBJEXT)

xcmod.$(LTOBJEXT): \
  vdwxc.$(LTOBJEXT) \
  precision.$(LTOBJEXT) \
  sys.$(LTOBJEXT)
