title: libGridXC User Guide

The library provides, in essence:

*  Routines to select the [functionals](02_api/functionals.html) to use.
*  Workhorse routines to compute the XC energies and potentials.
   See the [API description](02_api/index.html) for reference.

The library is written in modern Fortran. 

A typical serial program call is:

```fortran
  use precision, only : dp

  use gridxc, only: grid_p
  use gridxc,     only: gridxc_setXC_family_authors, gridxc_cellxc

  integer  :: nMesh(3), nSpin
  real(dp) :: cell(3,3), Dc, Dx, Ec, Ex, stress(3,3), 
  real(grid_p),allocatable :: dens(:,:,:,:), Vxc(:,:,:,:)
  
!    Find nSpin, cell(:,:), and nMesh(:)
  allocate( dens(nMesh(1),nMesh(2),nMesh(3),nSpin), &
             Vxc(nMesh(1),nMesh(2),nMesh(3),nSpin)) )

!    Find dens(:,:,:,:) at all mesh points
  call gridxc_setXC_family_authors( 1, 'GGA', 'PBE' )
  call gridxc_cellXC( 0, cell, nMesh, 1,nMesh(1), 1,nMesh(2), 1,nMesh(3), &
               nSpin, dens, Ex, Ex, Dx, Dc, stress, Vxc )
```

A typical parallel program call is:

```fortran
  use precision, only : dp

  use gridxc, only: grid_p
  use gridxc,     only: gridxc_setXC_family_authors, gridxc_cellxc
  
  integer  :: iSpin, myBox(2,3), nMesh(3), nSpin
  real(dp) :: cell(3,3), Dc, Dx, Ec, Ex, stress(3,3), 
  real(grid_p),allocatable :: dens(:,:,:,:), Vxc(:,:,:,:)

!    Find nSpin, cell(:,:), nMesh(:), and myBox(:,:)

 allocate( dens(myBox(1,1):myBox(2,1),        &
                 myBox(1,2):myBox(2,2),        &
                 myBox(1,3):myBox(2,3),nSpin), &
             Vxc(myBox(1,1):myBox(2,1),        &
                 myBox(1,2):myBox(2,2),        &
                 myBox(1,3):myBox(2,3),nSpin) )
  do i3 = myBox(1,3),myBox(2,3)
  do i2 = myBox(1,2),myBox(2,2)
  do i1 = myBox(1,1),myBox(2,1)
    do iSpin = 1,nSpin
      dens(i1,i2,i3,iSpin) =    !  (spin)density at point (i1,i2,i3)
    end do
  end do
  end do
  end do
  call gridxc_setXC_family_authors( 1, 'GGA', 'PBE' )
  call gridxc_cellXC( 0, cell, nMesh, myBox(1,1), myBox(2,1), &
                               myBox(1,2), myBox(2,2), &
                               myBox(1,3), myBox(2,3), &
               nSpin, dens, Ex, Ex, Dx, Dc, stress, Vxc )
```

For the radial grid interface, a typical call sequence is:

```fortran
   use ...
   integer  :: nr, nSpin
   real(dp) :: Dc, Dx, Ec, Ex
   real(dp),allocatable :: dens(:,:), rMesh(:,:), Vxc(:,:)
   
   !  (Find nr and nSpin)
   allocate( dens(nr,nSpin), rMesh(nr), Vxc(nr,nSpin) )

   ! (Find rMesh(:) and dens(:,:) at all mesh points)
   call gridxc_setXC_family_authors( 1, 'GGA', 'PBE' )
   call gridxc_atomXC( 0, nr, nr, rmesh, nSpin, Dens, Ex, Ec, Dx, Dc, Vxc )
```


