title: Installation

## Pre-requisites

* The [LibXC library](https://gitlab.com/libxc/libxc/-/tags) (optional but strongly recommended).

## Installation of libGridXC

Assuming that LibXC is installed in `${LIBXC_ROOT}` and that the
MPI-extended Fortran and C compilers are given by MPIFC and MPICC,
respectively, the following sequences of commands will install appropriate flavors of libGridXC in the
`${GRIDXC_ROOT}` installation directory:

### Serial version

```
    ./configure --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_ROOT}
    make
    make check
    make install
```

### MPI version

```
    FC=$MPIFC CC=$MPICC ../configure --with-mpi=${MPI_ROOT} --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_ROOT}
    make
    make check
    make install
```

### Single-precision version

This affects only the arrays handled by `gridxc_cellxc`. The spherical-grid interface always works in double-precision.

```
    ../configure  --enable-single-precision --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_ROOT}
    make
    make check
    make install
```

The above buildings will result in installation directories with the following structure:

```
${GRIDXC_ROOT}:
├── include
│   └── gridxc
│       ├── debugxc.mod
        .... (other .mod files)
│       └── xcmod.mod
├── lib
        ...
│   ├── libgridxc.a
        ...
```

so that to have different versions installed at the same time one
would have to specify different names (e.g., gridxc, gridxc-mpi,
gridxc-sp, gridxc-sp-mpi, etc) for the installation directory. An
alternative, more compact way, is presented in the next section.

### Multi-configuration installation

Using the `--enable-multiconfig` configure option, multiple
flavors of the library with different options can coexist in the
same root installation directory. For example,

```
    ./configure --enable-multiconfig --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_ROOT}
    make
    make check
    make install

    FC=$MPIFC CC=$MPICC ../configure --enable-multiconfig  --with-mpi=${MPI_ROOT} --with-libxc=${LIBXC_ROOT}  --prefix=${GRIDXC_ROOT}
    make
    make check
    make install
```

will result in

```
${GRIDXC_ROOT}:
├── include
│   ├── gridxc_dp
│   │   ├── gridxc.mod
        ...
│   └── gridxc_dp_mpi
│       ├── gridxc.mod
        ...
├── lib
        ...
│   ├── libgridxc_dp.a
│   ├── libgridxc_dp_mpi.a
```

This idea can be extended to the single/double precision options. The
script `multiconfig-build.sh` in the top level of the distribution can
be used to configure, compile, and install all available flavors of the library:
```
gridxc_dp
gridxc_dp_mpi
gridxc_sp
gridxc_sp_mpi
```

This is recommended procedure for production versions of the library. The script should be invoked with
the appropriate variables in the environment. For example: (note the escaped newlines):
```
FC=gfortran  MPIFC=mpif90  MPICC=mpicc \
LIBXC_ROOT=/path/to/libxc/installation \
MPI_ROOT=/path/to/mpi/installation \
GRIDXC_PREFIX=/path/to/desired/installation  sh multiconfig-build.sh
```

## Compiling user programs

Using the multi-config option discussed above, the appropriate modules
will be in `$GRIDXC_ROOT/include/gridxc_<flavor>`, and the library
file will be `$GRIDXC_ROOT/lib/libgridxc_<flavor>.a`, where `$GRIDXC_ROOT` is the value
of the `GRIDXC_PREFIX` installation option above.








