title: libGridXC API

[TOC]

Access to the library's functionality is provided by importing the [[gridxc(module)]] module:

```
use gridxc
```

which exports the interfaces to the public user-level routines and other symbols.

@note most routines and symbols
are **prefixed** by `gridxc` for export, whereas internally the `gridxc` prefix is not currently used. This means that
this API reference will have cross-references to routine names without the prefix.
@endnote

## Exported types and parameters

The library exports the kind parameter `grid_p` appropriate for the
arrays passed to a from the [[cellxc(proc)]] routine. This is
controlled by the setting of the `--enable-single-precision` configure
option (by default, a double-precision kind is returned).

## Initialization routine

It is necessary to call the [[gridxc_init(proc)]] routine before any other operations. Currently, this routine just
sets a few internal variables. If working in parallel, the MPI communicator is passed in this call.

## Selection of XC functionals

* The routine [[setxc(proc)]] and its simpler variants [[setxc_libxc_ids(proc)]] and [[setxc_family_authors(proc)]] select the
functionals to use.   The available functionals and the syntax for their
specification are discussed in the [Guide to functionals](./functionals.html).
This information is currently stored in global
module variables, and can be retrieved with the [[getXC(proc)]] routine.

## Main routines

### Atomic (spherical) grids

* Routine [[atomxc(proc)]] computes the exchange-correlation energies and potential for a spherical charge density, as given
  in a radial grid.

### Box (periodical) grids

* Routine [[cellxc(proc)]] is the major workhorse of the library. It
  computes the exchange-correlation energies and potential for a
  periodical charge density, represented in a (possibly distributed in parallel) parallepipedic grid.

## Other exported functionality

(See the source for full documentation)

### Convenience utilities

A few routines are exported for convenience to some user programs:

* nfft_gridxc: When VDW functionals are used, the library employs internally FFT routines that need the mesh sizes to be multiples of 2, 3, or 5. This routine (see the source) will help to determine the appropriate values.

* setmeshdistr/meshBox: These routines can be used to select an appropriate data distribution in parallel. See the source for more information. These are in the process of refactoring to make them truly stand-alone.

### Lower level entry points

* gridxc_ldaxc
* gridxc_ggaxc




